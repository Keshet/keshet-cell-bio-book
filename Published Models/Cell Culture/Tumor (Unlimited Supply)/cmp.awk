BEGIN { 

if (ARGC!=2) {
  print "wrong argument number:time (MCS)";
  exit -1;
 }
 
t=ARGV[1];

print "set term png size 1024,768; set output \"growthcmp.png\"; "\
      "set xlabel \"time/days\"; set ylabel \"tumor radius (um)\"; set grid;"\
      " plot \"logger_1.csv\" using 2:3 wi li title \"morpheus\","\
      " \"~/Daten/fitmulticell/tumor/experimental_data/processed_experimental_data_growth.csv\" "\
      " using 1:2:3 wi yerrorbars title \"exp\";" > "tmp.gpl"
system("gnuplot tmp.gpl");

fout=sprintf("ecm_%05d.csv", t);
fgraph=sprintf("ecmcmpday17.png", t);      

print sprintf("set term png size 1024,768; set output \"%s\"; "\
            "set xlabel \"distance to rim (um)\"; "\
            "set ylabel \"ECM.intensity.au\"; "\
            "set grid; "\
            "plot \"%s\" using ($1):($2) wi linesp title \"morpheus, t=%g days\","\
            " \"~/Daten/fitmulticell/tumor/experimental_data/processed_experimental_data_ecm.csv\""\
            " using 1:2:3 wi yerrorbars title \"exp, t=%g days\";"\
            ,fgraph,fout,t/24,17) > "tmp.gpl"
system("gnuplot tmp.gpl");
            
fout=sprintf("prolif_ratio_%05d.csv", t);
fgraph=sprintf("prolifcmpday17.png", t);            
            
print sprintf("set term png size 1024,768; set output \"%s\"; "\
            "set xlabel \"distance to rim (um)\"; "\
            "set ylabel \"ratio of proliferating cells\"; "\
            "set grid; plot \"%s\" using ($1):($2) wi linesp title \"morpheus, t=%g days\","\
            " \"~/Daten/fitmulticell/tumor/experimental_data/processed_experimental_data_prolif.csv\""\
            " using 1:2:3 wi yerrorbars title \"exp, t=%g days\";"\
            ,fgraph,fout,t/24,17) > "tmp.gpl"
system("gnuplot tmp.gpl");

            
}
