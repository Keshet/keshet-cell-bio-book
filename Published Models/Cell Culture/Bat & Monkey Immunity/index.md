---
MorpheusModelID: M7677
hidden: true
private: true

authors: [C. E. Brook, M. Boots, K. Chandran, A. P. Dobson, C. Drosten, A. L. Graham, B. T. Grenfell, M. A. Müller, M. Ng, L.-F. Wang, A. van Leeuwen]
contributors: [C. Munegowda, M. Japakhova, S. Singhal, J. Bürger, L. Brusch]

title: "Viral dynamics in monkey and bat cell lines"

# Reference details
publication:
  doi: "10.7554/eLife.48401"
  title: "Accelerated viral dynamics in bat cell lines, with implications for zoonotic emergence"
  journal: "eLife"
  volume: 9
  page: "e48401"
  year: 2020
  original_model: false

tags:
- 2D
- Bat
- Cellular Automaton Model
- Constitutive Immunity
- Deterministic Model
- Immunity
- Induced Immunity 
- Interferon 
- Mean-field Model
- MOI
- Monkey
- Spatial Model
- Stochastic Model
- Viral Dynamics
- Zoonotic Emergence

categories:
- DOI:10.7554/eLife.48401
---

## Introduction

[Brook _et al._](#reference) studied the effect of distinct immune phenotypes on cellular-scale viral propagation in representative mammalian kidney cell lines with differing interferon pathways. Differences in the IFN response upon pathogen invasion allowed to experimentally observe, model and numerically simulate viral dynamics in cells with 

1. absent, 
2. induced, and 
3. constitutive immunity. 

[Brook _et al._](#reference) also developed two related models of these processes, (1) a **mean field compartmental model** for within-host viral dynamics under different immunity assumptions and (2) a **stochastic, spatially explicit individual-based model**. The mean field model was fitted to experimental data obtained in a series of _in vitro_ infection trials. The model results showed that stronger antiviral immune response restrains cell death allowing for an accelerated spread and longer term viral presence within the host. The finding suggests a role for a pathogen reservoir in hosts with stronger immunity. 

## Experimental Data

[Brook _et al._](#reference) have performed a series of infection assays on three immortalized mammalian cell lines:

1. IFN-defective Vero (African green monkey) cells,
2. RoNi/7.1 (Bat _Rousettus aegyptiacus_) cells with an induced IFN response upon infection, and 
3. PaKiT01 (Bat _Pteropus alecto_) cells that constitutively express IFN-$\alpha$, i.e. some amount of antiviral cells is present even at the disease-free equilibrium. 

The three cell lines were infected with three replication competent, GFP-tagged vesicular stomatitis Indiana viruses: 

- rVSV-G, 
- rVSV-EBOV, and 
- rVSV-MARV. 

Cell entry was mediated by the glycoprotein of the bat-evolved filoviruses, Ebola (EBOV) and Marburg (MARV).
Each cell culture was infected with each virus at two multiplicities of infection (MOI): 0.0001 (low) and 0.001 (high). At the start of an infectivity assay $k$ viral particles were introduced to the cell monolayer. Proportion of infected cells is described by the Poisson distribution: 

$$ \begin{align}
  P(k) = \frac{e^{m}m^{k}}{k!}
\end{align} $$

where $m$ is multiplicity of infection (MOI) and takes either of the two values \{0.001, 0.0001\}. Assuming that 90\% confluent cell culture at the beginning of each trial contained $\sim 9 \times 10^{5}$ cells, each trial started from introducing the virus to $\sim 81$ ($\mathrm{MOI} = 0.0001$) or $\sim 810$ ($\mathrm{MOI} = 0.001$) cells. Each infectivity assay was carried out on a 6-well plate. Each plate contained a cell line infected with a virus at different levels of $\mathrm{MOI}$. 1 control well received no viral particles, 2-3 wells were each infected with a virus at $\mathrm{MOI} = 0.001$ or $\mathrm{MOI} = 0.0001$. 

## Model Description

[Brook _et al._](#reference) assumed the cells to have either of the following five states: 
- susceptible (S)
- antiviral (A)
- exposed (E)
- infected (I)
- dead (D)

The viral infection and immunity processes are summarized in this state transition graph:

![Cell State Transitions](Cell_State_Transitions.png "Overview of cell state transitions")

The attached Morpheus model is twofold, it comprises the mean field model and the spatial model, running in parallel for the same parameter set. The parameter set is taken from the publication's [Table 1](https://elifesciences.org/articles/48401/figures#table1) and [Suppl. file 4](https://elifesciences.org/articles/48401/figures#supp4). The additional model constants termed

1. `select.celltype`  
2. `select.virus` 
3. `select.immunity`
4. `MOI`

allow to readily parametrize and run any of the $3 \times 3 \times 3 \times 2 = 54$ specific combinations of cell line/virus/immune type/virus load, all in the same model. Hence, only these select statements need to be set and no manual change of parameter values is required for any of the model variants to run correctly.

Note that [Brook _et al._](#reference) had used slightly differing values for the cell death rate $\mu$ in the mean field and spatial models, apparently due to dropping versus rounding the decimals, and the Morpheus model copied these slight differences.

Importantly, [Brook _et al._](#reference) swapped the value of $\mu$ between (1) Vero and (3) PaKiT01 cell lines in the published simulation code of the spatial model, which was not reported as such in [Table 1](https://elifesciences.org/articles/48401/figures#table1) nor anywhere else in the publication. In the Morpheus model, this swapping of parameter values $\mu_\text{Vero}=\frac{1}{83}, \mu_\text{PaKiT01}=\frac{1}{120}$ is copied from the published simulation code. This differs from [Table 1](https://elifesciences.org/articles/48401/figures#table1) and [Suppl. file 4](https://elifesciences.org/articles/48401/figures#supp4) where $\mu_\text{Vero}=\frac{1}{121}, \mu_\text{PaKiT01}=\frac{1}{84}$ are stated for both (mean field and spatial) models. 

### Mean Field Model
For the mean field model, the state of the system was described by proportions of the respective cell types, e.g.,
$ P_\mathrm{S} = 0.2 $
would mean that of the total cell count 1/5 were susceptible. Furthermore, the authors approximated the IFN particle dynamics by defining a rate of cell progression from susceptible to antiviral (denoted by $\rho$), which depends on the total number of exposed cells.

This resulted in the following system of ODEs:

$$\begin{align}
  \frac{dP_\mathrm{S}}{dt} &= bP_\mathrm{D}(P_\mathrm{S} + P_\mathrm{A}) - \beta P_\mathrm{S}P_\mathrm{I} - \mu P_\mathrm{S} -\rho P_\mathrm{E}P_\mathrm{S} -\epsilon P_\mathrm{S} - cP_\mathrm{A} \\\\
  \frac{dP_\mathrm{A}}{dt} &= \rho P_\mathrm{E}P_\mathrm{S} + \epsilon P_\mathrm{S} - cP_\mathrm{A} - \mu P_\mathrm{A} \\\\
  \frac{dP_\mathrm{E}}{dt} &= \beta P_\mathrm{S}P_\mathrm{I} - \sigma P_\mathrm{E} - \mu P_\mathrm{E} \\\\
  \frac{dP_\mathrm{I}}{dt} &= \sigma P_\mathrm{E} - \alpha P_\mathrm{I} - \mu P_\mathrm{I} \\\\
  \frac{dP_\mathrm{D}}{dt} &= \mu (P_\mathrm{S} + P_\mathrm{I} + P_\mathrm{E} + P_\mathrm{A}) + \alpha P_\mathrm{I} - bP_\mathrm{D}(P_\mathrm{S} + P_\mathrm{A})
\end{align} $$

Initial conditions are calculated (including the dependency of cell types at disease-free equilibrium DFE) exactly and individually for each combination of cell line/virus/immune type as defined in the mean field model code by [Brook _et al._](#reference).

### Spatial Model
The spatial model is a cellular automaton set up on a fixed two-dimensional hexagonal grid with $10^{4}$ cells and cell-cell interactions due to fractions of cell types are computed for each cell in its 6-cell and 36-cell neighborhood, as defined by [Brook _et al._](#reference). The `time` unit is hours post-infection and the `space` unit is one lattice interval corresponding to the size of a cell.

Regarding the transition from susceptible to antiviral cells, the important parameters of the model, $\rho$ (induced immunity) or $\epsilon$ (constitutive immunity), are respectively taken from the mean field model and in each ten-minute time step multiplied with, respectively, the proportion of exposed and susceptible cells. [Brook _et al._](#reference) set the remaining waiting times by sampling from a normal distribution around the inverse of each parameter.

Transmission of the virus and the birth of new cells is computed probabilistically. The birth rate $b$ is multiplied by the fraction of susceptible cells in the 6-cell neighborhood of a dead cell. In contrast, the transmission rate $\beta$ is multiplied by the proportion of infected cells in the 36-cell neighborhood of a susceptible cell.

To compensate for the difference between local interactions in the spatial model and globally averaged interactions in the mean field model, [Brook _et al._](#reference) rescaled two parameter values for the spatial model: the $b_\mathrm{MF}$ value from the mean field model was multiplied by $6$, and $\beta_\mathrm{MF}$ was multiplied by $10$.

Initial conditions are copied exactly and individually for each combination of cell line/virus/immune type from the corresponding spatial model code by [Brook&nbsp;_et&nbsp;al._](#reference).

## Results
We have reproduced the graphs (and share our plotting workflow in the iPython Notebook under Downloads below) of the publication using Morpheus and based on the author's code in the supplementary materials. To rerun or extend each of our results, go to `Global` in Morpheus and specify the desired cell, virus and immunity type.

### Reproduction of mean field results

![](paper_Fig5.png "Results of the mean field model as published by [Brook _et al._](#reference) ([Fig. 5](https://elifesciences.org/articles/48401#fig5)) [*CC BY 4.0*](https://creativecommons.org/licenses/by/4.0/)")

![Results for the mean field model reproduced in Morpheus.](model_mf.png "Results for the mean field model reproduced in Morpheus")

Comparing both figures above, we could reproduce the mean field model exactly, as expected for this deterministic model.
Additional curves in the Morpheus graphs show the time courses of all remaining variables.

Note that the black dots in the publication's graphs correspond to experimental data and are not included in the Morpheus graphs.

### Reproduction of spatial model results

![](paper_Fig5Suppl3.png "Results of the spatial model as published by [Brook _et al._](#reference) ([Fig. 5 Suppl. 3](https://elifesciences.org/articles/48401/figures#fig5s3)). Unshaded panels correspond to best fitting immune type for each virus/cell line combination and only those are followed up in the Morpheus model below. [*CC BY 4.0*](https://creativecommons.org/licenses/by/4.0/)")

![Statistics over ten simulations of stochastic spatial model simulations in Morpheus](model_spatial.png "Statistics over ten simulations of stochastic spatial model simulations in Morpheus")

Again for the Morpheus model, all curves show time courses for individual cell types. The published spatial model results were reproduced statistically given ten simulation runs at each combination. A representative run can be followed in the video below.

### Example Simulation

![](published-vs-reproduced.mp4)

This synced composite video shows the published result on top and the reproduced result in Morpheus at the bottom. 
Top: Example run of the spatial model from [Brook _et al._](#reference) with PaKiT01 cell type, rVSV-EBOV virus and constitutive immunity, published as [Video 3](https://elifesciences.org/articles/48401#video3) ([*CC BY 4.0*](https://creativecommons.org/licenses/by/4.0/)).
Bottom: Example run of the spatial model in Morpheus with PaKiT01 cell type, rVSV-EBOV virus and constitutive immunity. To run this combination with the present model.xml, change the three `select...` constants in the Morpheus GUI from (1,1,1) to (3,2,3). As we randomly initialize $10$ among $10^{4}$ cells as exposed (according to $\mathrm{MOI} = 0.001$), those positions and the shapes of the ensuing waves will occur differently each time and do not match the published video. Color code for cells and curves roughly matches that of the publication, see common color legend at the right margin.

