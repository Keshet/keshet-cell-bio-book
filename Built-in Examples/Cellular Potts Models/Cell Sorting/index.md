---
MorpheusModelID: M0021

authors: [F. Graner, J. A. Glazier]

title: "Differential Adhesion: Cell Sorting in Two Dimensions"
date: "2019-11-06T15:57:00+01:00"
lastmod: "2020-10-30T12:37:00+01:00"

aliases: [/examples/differential-adhesion-cell-sorting-in-two-dimensions/]

menu:
  Built-in Examples:
    parent: Cellular Potts Models
    weight: 10
weight: 110
---

## Introduction

This model shows the original cellular Potts model (a.k.a. [Glazier-Graner model][graner-1992]) of cell sorting based on the Steinberg's differential adhesion hypothesis.

![](cellsorting-2d.png "Yellow cells engulf the red cells as a result of differential adhesion.")

## Description

Two ```CellTypes``` are defined, each of which has a ```VolumeConstraint``` specifying the cell's target area/volume. In the ```CPM``` element, the ```MetropolisKinetics``` can be configured and the ```Interaction``` energies between cell types are specified.

Although cells can be initialized as single points using e.g. the ```InitCircle``` plugin, in this example, the ```Nodes``` of each ```Cell``` in the ```CellPopulations``` are given explicitly. In fact, these ```Populations``` are restored results of a previous simulation. 

The simulation shows two populations of spatially resolved cells that initially organized in a mosaic fashion. Through differential adhesion, the motile cells sort out and re-organize into an distribution in which one cell type engulfes the other.

Snapshots of the simulation are saved to files named ```[Title][Time].xml.gz```. These files containing intermediate and result states can be opened and used as initial conditions for new simulations. Remember to change ```StartTime``` and ```StopTime``` accordingly.

<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/47171579?loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## Reference

F. Graner, J. A. Glazier: [Simulation of biological cell sorting using a two-dimensional extended Potts model][graner-1992]. *Phys. Rev. Lett.* **69** (13): 2013-2016, 1992.

[graner-1992]: http://dx.doi.org/10.1103%2FPhysRevLett.69.2013